#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:31325484:7dcba08c1203d651e04eb8af3bee6a1413fe92f4; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:29365544:29896184fff2b6a2fe0be70670b9497eb24e6e5f EMMC:/dev/block/bootdevice/by-name/recovery 7dcba08c1203d651e04eb8af3bee6a1413fe92f4 31325484 29896184fff2b6a2fe0be70670b9497eb24e6e5f:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
